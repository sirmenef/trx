import { Component } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private http: HttpClient, private router: Router) {

  }
  reg = {
    trxId: /^[\d]{2,6}$/,
    bankCode: /^[\d]{3}$/};
  trxId: FormControl = new FormControl('', Validators.required);
  res: any[];
  bankCode: FormControl = new FormControl('', Validators.required);

  onSubmit() {
    if (this.reg.trxId.test(this.trxId.value) && this.reg.bankCode.test(this.bankCode.value)) {
      this.http.get(`api/transaction/${this.trxId.value}/${this.bankCode.value}`).subscribe(
        data => {
          if (data) {
            this.res = data as any[];
            this.router.navigate(['/transaction']);
          }
        },
        () => {
        }
      );
    }
  }
}
